<?php

declare(strict_types=1);

return [
    "proxmox_host" => env("PROXMOX_HOST"),
    "proxmox_username" => env("PROXMOX_USERNAME"),
    "proxmox_token_name" => env("PROXMOX_TOKEN_NAME"),
    "proxmox_token_secret" => env("PROXMOX_TOKEN_SECRET"),
];
