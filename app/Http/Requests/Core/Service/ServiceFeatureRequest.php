<?php

declare(strict_types=1);

namespace App\Http\Requests\Core\Service;

use Illuminate\Foundation\Http\FormRequest;

final class ServiceFeatureRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "name" => "required|string",
            "value" => "required",
            "type" => "required"
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
