<?php

declare(strict_types=1);

namespace App\Http\Requests\Core\Service;

use Illuminate\Foundation\Http\FormRequest;

final class ServiceFeaturePriceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "price" => "required"
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
