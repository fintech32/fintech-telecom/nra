<?php

declare(strict_types=1);

namespace App\Http\Requests\Core\Service;

use Illuminate\Foundation\Http\FormRequest;

final class ServiceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "name"          => "required|string",
            "description"   => "required|string",
            "low_price"     => "required",
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
