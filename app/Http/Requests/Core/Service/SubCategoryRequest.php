<?php

declare(strict_types=1);

namespace App\Http\Requests\Core\Service;

use Illuminate\Foundation\Http\FormRequest;

final class SubCategoryRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "name" => "required|string",
            "description" => "string"
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
