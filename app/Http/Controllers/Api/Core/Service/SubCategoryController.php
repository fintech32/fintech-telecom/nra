<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Core\Service;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Core\Service\SubCategoryRequest;
use App\Models\Core\Subcategory;
use Illuminate\Http\Request;
use Exception;

final class SubCategoryController extends ApiController
{
    public function index($category_id)
    {
        try {
            $subcategories = Subcategory::where('category_id', $category_id)->get();

            return $this->sendSuccess(null, compact('subcategories'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function store($category_id, SubCategoryRequest $request)
    {
        try {
            $request->merge(['category_id' => $category_id]);
            $sub = Subcategory::create($request->all());
            return $this->sendSuccess("Sous Catégorie {$sub->name} créer avec succès", compact('sub'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function show($category_id, $sub_id)
    {
        try {
            $sub = Subcategory::find($sub_id);
            return $this->sendSuccess(null, compact('sub'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function update($category_id, $sub_id, Request $request)
    {
        try {
            $sub = Subcategory::find($sub_id);
            $sub->update($request->all());
            return $this->sendSuccess("Sous catégorie {$sub->name} mise à jours", compact('sub'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function destroy($category_id, $sub_id)
    {
        try {
            $sub = Subcategory::find($sub_id);
            $sub->delete();
            return $this->sendSuccess("Sous catégorie {$sub->name} supprimé", null);
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
