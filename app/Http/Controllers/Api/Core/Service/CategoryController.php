<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Core\Service;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Core\Service\CategoryRequest;
use App\Models\Core\Category;
use Illuminate\Http\Request;
use Exception;

final class CategoryController extends ApiController
{
    public function index()
    {
        $categories = Category::all();
        return $this->sendSuccess(null, compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        try {
            $category = Category::create($request->all());
            return $this->sendSuccess("Catégorie {$category->name} créer avec succès", compact('category'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function show($category_id)
    {
        try {
            $category = Category::find($category_id);
            return $this->sendSuccess(null, compact('category'));
        } catch (Exception $exception) {
            return  $this->sendError($exception);
        }
    }

    public function update($category_id, Request $request)
    {
        try {
            $category = Category::find($category_id);
            $category->update($request->all());
            return $this->sendSuccess("Categorie {$category->name} mise à jours", compact('category'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function destroy($category_id)
    {
        try {
            $category = Category::find($category_id);
            $category->delete();
            return $this->sendSuccess("Categorie {$category->name} supprimé", compact('category'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
