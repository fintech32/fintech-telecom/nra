<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Core\Service;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Core\Service\ServiceFeatureRequest;
use App\Models\Core\Service;
use App\Models\Core\ServiceCaracteristique;
use Illuminate\Http\Request;
use Exception;

final class ServiceFeatureController extends ApiController
{
    public function index($service_id)
    {
        try {
            $features = Service::find($service_id)->features;
            return $this->sendSuccess(null, compact('features'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function store($service_id, ServiceFeatureRequest $request)
    {
        try {
            $request->merge(['service_id' => $service_id]);
            $feature = Service::find($service_id)->features()->create($request->all());
            return $this->sendSuccess("Nouvelle feature ajouté: {$feature->name}", compact('feature'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function show($service_id, $feature_id)
    {
        try {
            $feature = ServiceCaracteristique::find($feature_id);
            return $this->sendSuccess(null, compact('feature'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function update($service_id, $feature_id, Request $request)
    {
        try {
            $feature = ServiceCaracteristique::find($feature_id);
            $feature->update($request->all());
            return $this->sendSuccess("Modification de la feature: {$feature->name}", compact('feature'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function destroy($service_id, $feature_id)
    {
        try {
            $feature = ServiceCaracteristique::find($feature_id);
            $feature->delete();
            return $this->sendSuccess("Suprpession de la feature: {$feature->name}");
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
