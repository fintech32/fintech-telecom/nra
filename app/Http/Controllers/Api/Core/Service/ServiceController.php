<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Core\Service;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Core\Service\ServiceRequest;
use App\Models\Core\Service;
use Illuminate\Http\Request;
use Exception;

final class ServiceController extends ApiController
{
    public function index()
    {
        try {
            $services = Service::all();
            return $this->sendSuccess(null, compact('services'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function store(ServiceRequest $request)
    {
        try {
            $service = Service::create($request->all());
            return $this->sendSuccess("Service {$service->name} créer", compact('service'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function show($service_id)
    {
        try {
            $service = Service::find($service_id);
            return $this->sendSuccess(null, compact('service'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function update(Request $request, $service_id)
    {
        try {
            $service = Service::find($service_id);
            $service->update($request->all());
            return $this->sendSuccess("Service {$service->name} modifier", compact('service'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function destroy($service_id)
    {
        try {
            $service = Service::find($service_id);
            $service->delete();
            return $this->sendSuccess("Service {$service->name} supprimer");
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
