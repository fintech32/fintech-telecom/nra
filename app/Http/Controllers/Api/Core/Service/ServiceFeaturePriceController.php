<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Core\Service;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Core\Service\ServiceFeaturePriceRequest;
use App\Models\Core\ServiceCaracteristique;
use Illuminate\Http\Request;
use Exception;

final class ServiceFeaturePriceController extends ApiController
{
    public function index($service_id, $feature_id)
    {
        try {
            $price = ServiceCaracteristique::find($feature_id)->price;
            return $this->sendSuccess(null, compact('price'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function store($service_id, $feature_id, ServiceFeaturePriceRequest $request)
    {
        try {
            $request->merge(['caracteristique_id' => $feature_id]);
            $price = ServiceCaracteristique::find($feature_id)->price;
            $price->create($request->all());
            return $this->sendSuccess("Création d'un tarif: ".$price->caracteristique->name, compact('price'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function show($service_id, $feature_id)
    {
        try {
            $price = ServiceCaracteristique::find($feature_id)->price;
            return $this->sendSuccess(null, compact('price'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function update($service_id, $feature_id, $price_id, Request $request)
    {
        try {
            $price = ServiceCaracteristique::find($feature_id)->price;
            $price->update($request->all());
            return $this->sendSuccess("Mise à jour d'un tarif: ".$price->caracteristique->name, compact('price'));
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function destroy($service_id, $feature_id, $price_id)
    {
        try {
            $price = ServiceCaracteristique::find($feature_id)->price;
            $price->delete();
            return $this->sendSuccess("Suppression d'un tarif: ".$price->caracteristique->name);
        } catch (Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
