<?php

declare(strict_types=1);

namespace App\Models\Gestion;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Gestion\Order
 *
 * @property int $id
 * @property string $date
 * @property string $expirationDate
 * @property float $price_ht
 * @property float $price_taxe
 * @property float $price_ttc
 * @property int $customer_id
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceTaxe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceTtc($value)
 * @mixin \Eloquent
 */
final class Order extends Model
{
}
