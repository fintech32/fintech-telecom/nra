<?php

declare(strict_types=1);

namespace App\Models\Gestion;

use App\Models\Crm\CustomerPaymentMethod;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Gestion\InvoicePayment
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $paymentDate
 * @property string $paymentType
 * @property string|null $paymentIdentifier
 * @property int|null $customer_payment_method_id
 * @property int $invoice_id
 * @property-read \App\Models\Gestion\Invoice $invoice
 * @property-read CustomerPaymentMethod|null $method
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment whereCustomerPaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment wherePaymentIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoicePayment wherePaymentType($value)
 * @mixin \Eloquent
 */
final class InvoicePayment extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['paymentDate'];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function method()
    {
        return $this->belongsTo(CustomerPaymentMethod::class);
    }
}
