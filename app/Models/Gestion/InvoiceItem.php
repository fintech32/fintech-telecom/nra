<?php

declare(strict_types=1);

namespace App\Models\Gestion;

use App\Models\Crm\CustomerService;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Gestion\InvoiceItem
 *
 * @property int $id
 * @property string $description
 * @property int $qte
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $period_start
 * @property \Illuminate\Support\Carbon|null $period_end
 * @property int $invoice_id
 * @property int $customer_service_id
 * @property-read \App\Models\Gestion\Invoice $invoice
 * @property-read CustomerService $service
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem whereCustomerServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem wherePeriodEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem wherePeriodStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceItem whereQte($value)
 * @mixin \Eloquent
 */
final class InvoiceItem extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['period_start', 'period_end'];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function service()
    {
        return $this->belongsTo(CustomerService::class);
    }
}
