<?php

declare(strict_types=1);

namespace App\Models\Gestion;

use App\Models\Crm\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Gestion\Invoice
 *
 * @property int $id
 * @property string $invoice_number Format: FR-YYYY-999999
 * @property float $price_ht
 * @property float $price_tva
 * @property float $price_ttc
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $order_id
 * @property-read Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePriceHt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePriceTtc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePriceTva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Invoice extends Model
{
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
