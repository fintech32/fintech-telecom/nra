<?php

declare(strict_types=1);

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Core\ServiceCaracteristique
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $type Par example: memory pour memoire
 * @property int $service_id
 * @property-read \App\Models\Core\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristique whereValue($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Core\ServiceCaracteristiquePrice|null $price
 */
final class ServiceCaracteristique extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function price(): HasOne
    {
        return $this->hasOne(ServiceCaracteristiquePrice::class);
    }
}
