<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Documentation;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Documentation\DocArticle
 *
 * @property int $id
 * @property string $code_article Format: KB999999999
 * @property string $objectif
 * @property string $prerequis
 * @property string $pratique
 * @property int $count_view
 * @property int $article_util
 * @property int $article_non_util
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $doc_sub_category_id
 * @property-read \App\Models\Core\IT\Documentation\DocSubCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereArticleNonUtil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereArticleUtil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereCodeArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereCountView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereDocSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereObjectif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle wherePratique($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle wherePrerequis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class DocArticle extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(DocSubCategory::class, 'doc_sub_category_id');
    }
}
