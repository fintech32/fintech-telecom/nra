<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Documentation;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Documentation\DocCategory
 *
 * @property int $id
 * @property string $title
 * @property int $doc_database_id
 * @property-read \App\Models\Core\IT\Documentation\DocDatabase|null $database
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory whereDocDatabaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocCategory whereTitle($value)
 * @mixin \Eloquent
 */
final class DocCategory extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function database()
    {
        return $this->belongsTo(DocDatabase::class);
    }
}
