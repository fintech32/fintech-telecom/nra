<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Documentation;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Documentation\DocSubCategory
 *
 * @property int $id
 * @property string $title
 * @property int|null $parent_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $doc_category_id
 * @property-read \App\Models\Core\IT\Documentation\DocCategory|null $category
 * @property-read DocSubCategory|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereDocCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocSubCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class DocSubCategory extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(DocCategory::class);
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
}
