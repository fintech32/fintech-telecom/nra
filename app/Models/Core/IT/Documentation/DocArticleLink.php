<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Documentation;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Documentation\DocArticleLink
 *
 * @property int $id
 * @property int $article_link_id
 * @property int $doc_article_id
 * @property-read \App\Models\Core\IT\Documentation\DocArticle $article
 * @property-read \App\Models\Core\IT\Documentation\DocArticle $link
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink whereArticleLinkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink whereDocArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocArticleLink whereId($value)
 * @mixin \Eloquent
 */
final class DocArticleLink extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function article()
    {
        return $this->belongsTo(DocArticle::class, 'doc_article_id');
    }

    public function link()
    {
        return $this->belongsTo(DocArticle::class, 'article_link_id');
    }
}
