<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Documentation;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Documentation\DocDatabase
 *
 * @property int $id
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|DocDatabase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocDatabase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocDatabase query()
 * @method static \Illuminate\Database\Eloquent\Builder|DocDatabase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DocDatabase whereTitle($value)
 * @mixin \Eloquent
 */
final class DocDatabase extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
