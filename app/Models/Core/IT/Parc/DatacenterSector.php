<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Parc\DatacenterSector
 *
 * @property int $id
 * @property int $dedicated_server
 * @property int $dedicated_server_eco
 * @property int $vps
 * @property int $nas_ha
 * @property int $cloud_file
 * @property int $ip_address
 * @property int $load_balancer
 * @property int $vrack
 * @property int $ssl_gateway
 * @property int $hebergement
 * @property int $domain
 * @property int $cloud_db
 * @property int $email
 * @property int $email_pro
 * @property int $collaboration
 * @property int $office
 * @property int $dns_hosting
 * @property int $dns_anycast
 * @property int $cdn
 * @property int $voip_sip
 * @property int $voip_interconnect
 * @property int $voip_telephony
 * @property int $internet_adsl
 * @property int $internet_ftth
 * @property int $datacenter_id
 * @property-read \App\Models\Core\IT\Parc\Datacenter $datacenter
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector query()
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereCdn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereCloudDb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereCloudFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereCollaboration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDatacenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDedicatedServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDedicatedServerEco($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDnsAnycast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDnsHosting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereEmailPro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereHebergement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereInternetAdsl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereInternetFtth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereLoadBalancer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereNasHa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereSslGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereVoipInterconnect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereVoipSip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereVoipTelephony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereVps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DatacenterSector whereVrack($value)
 * @mixin \Eloquent
 */
final class DatacenterSector extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function datacenter()
    {
        return $this->belongsTo(Datacenter::class);
    }
}
