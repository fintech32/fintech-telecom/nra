<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Parc\Datacenter
 *
 * @property int $id
 * @property string $name
 * @property string $city
 * @property string $primary_ip
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Core\IT\Parc\DatacenterSector|null $sector
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter query()
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter wherePrimaryIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Datacenter whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Datacenter extends Model
{
    protected $guarded = [];

    public function sector()
    {
        return $this->hasOne(DatacenterSector::class);
    }
}
