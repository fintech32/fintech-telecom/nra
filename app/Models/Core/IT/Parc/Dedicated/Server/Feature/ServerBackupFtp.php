<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server\Feature;

use App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerBackupFtp
 *
 * @property-read ServerSystem|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|ServerBackupFtp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerBackupFtp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerBackupFtp query()
 * @mixin \Eloquent
 */
final class ServerBackupFtp extends Model
{
    protected $guarded = [];
    public $timestamps = false;


    public function server(): BelongsTo
    {
        return $this->belongsTo(ServerSystem::class, 'server_system_id');
    }
}
