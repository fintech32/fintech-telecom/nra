<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server\Feature;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerIpmiAccess
 *
 * @property-read \App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerIpmi|null $ipmi
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmiAccess newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmiAccess newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmiAccess query()
 * @mixin \Eloquent
 */
final class ServerIpmiAccess extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function ipmi(): BelongsTo
    {
        return $this->belongsTo(ServerIpmi::class, 'server_ipmi_id');
    }
}
