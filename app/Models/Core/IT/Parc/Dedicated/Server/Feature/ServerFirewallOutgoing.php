<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server\Feature;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerFirewallOutgoing
 *
 * @property-read \App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerFirewall|null $firewall
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewallOutgoing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewallOutgoing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewallOutgoing query()
 * @mixin \Eloquent
 */
final class ServerFirewallOutgoing extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function firewall(): BelongsTo
    {
        return $this->belongsTo(ServerFirewall::class, 'server_firewall_id');
    }
}
