<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server\Feature;

use App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerIpmi
 *
 * @property-read ServerSystem|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIpmi query()
 * @mixin \Eloquent
 */
final class ServerIpmi extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function server(): BelongsTo
    {
        return $this->belongsTo(ServerSystem::class, 'system_server_id');
    }
}
