<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server\Feature;

use App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\Feature\ServerFirewall
 *
 * @property-read ServerSystem|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewall newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewall newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerFirewall query()
 * @mixin \Eloquent
 */
final class ServerFirewall extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function server(): BelongsTo
    {
        return $this->belongsTo(ServerSystem::class, 'server_system_id');
    }
}
