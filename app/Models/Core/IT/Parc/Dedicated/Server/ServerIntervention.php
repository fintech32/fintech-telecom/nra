<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\ServerIntervention
 *
 * @property-read \App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIntervention newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIntervention newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerIntervention query()
 * @mixin \Eloquent
 */
final class ServerIntervention extends Model
{
    protected $guarded = [];

    public function server(): BelongsTo
    {
        return $this->belongsTo(ServerSystem::class, 'server_system_id');
    }
}
