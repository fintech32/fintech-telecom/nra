<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\ServerAuthentification
 *
 * @property-read \App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem|null $server
 * @method static \Illuminate\Database\Eloquent\Builder|ServerAuthentification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerAuthentification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerAuthentification query()
 * @mixin \Eloquent
 */
final class ServerAuthentification extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function server(): BelongsTo
    {
        return $this->belongsTo(ServerSystem::class, 'server_system_id');
    }
}
