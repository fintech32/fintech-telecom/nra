<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Server;

use App\Models\Core\IT\Parc\Datacenter;
use App\Models\Crm\CustomerService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Server\ServerSystem
 *
 * @property-read CustomerService|null $customerservice
 * @property-read Datacenter $datacenter
 * @method static \Illuminate\Database\Eloquent\Builder|ServerSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServerSystem query()
 * @mixin \Eloquent
 */
final class ServerSystem extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function datacenter(): BelongsTo
    {
        return $this->belongsTo(Datacenter::class, 'datacenter_id');
    }

    public function customerservice(): BelongsTo
    {
        return $this->belongsTo(CustomerService::class, 'customer_service_id');
    }
}
