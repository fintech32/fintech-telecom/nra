<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Nas;

use App\Models\Core\IT\Parc\Datacenter;
use App\Models\Crm\CustomerService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Nas\NasSystem
 *
 * @property-read CustomerService|null $customerservice
 * @property-read Datacenter $datacenter
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystem query()
 * @mixin \Eloquent
 */
final class NasSystem extends Model
{
    protected $guarded = [];

    public function customerservice(): BelongsTo
    {
        return $this->belongsTo(CustomerService::class, 'customer_service_id');
    }

    public function datacenter(): BelongsTo
    {
        return $this->belongsTo(Datacenter::class, 'datacenter_id');
    }
}
