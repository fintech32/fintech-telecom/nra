<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Parc\Dedicated\Nas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Core\IT\Parc\Dedicated\Nas\NasSystemPartition
 *
 * @property-read \App\Models\Core\IT\Parc\Dedicated\Nas\NasSystem|null $nas
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystemPartition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystemPartition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NasSystemPartition query()
 * @mixin \Eloquent
 */
final class NasSystemPartition extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function nas(): BelongsTo
    {
        return $this->belongsTo(NasSystem::class, 'nas_system_id');
    }
}
