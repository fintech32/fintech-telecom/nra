<?php

declare(strict_types=1);

namespace App\Models\Core\IT;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\NiveauSupportSla
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property int $hour_ticket_relevance
 * @property int $hour_reso_relevance
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla query()
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla whereHourResoRelevance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla whereHourTicketRelevance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|NiveauSupportSla wherePrice($value)
 * @mixin \Eloquent
 */
final class NiveauSupportSla extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
