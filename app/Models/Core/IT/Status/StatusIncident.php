<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusIncident
 *
 * @property int $id
 * @property string $step
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status_event_id
 * @property-read \App\Models\Core\IT\Status\StatusEvent|null $event
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereStatusEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusIncident whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class StatusIncident extends Model
{
    protected $guarded = [];

    public function event()
    {
        return $this->belongsTo(StatusEvent::class);
    }
}
