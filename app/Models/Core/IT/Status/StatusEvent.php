<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusEvent
 *
 * @property int $id
 * @property string $title
 * @property string $impact
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $monitoring_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status_component_id
 * @property-read \App\Models\Core\IT\Status\StatusComponent|null $component
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereImpact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereMonitoringAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereStatusComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusEvent whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class StatusEvent extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'monitoring_at'];

    public function component()
    {
        return $this->belongsTo(StatusComponent::class);
    }
}
