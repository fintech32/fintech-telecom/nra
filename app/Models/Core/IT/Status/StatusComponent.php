<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusComponent
 *
 * @property int $id
 * @property string $title
 * @property string $status
 * @property int|null $parent_id
 * @property int $status_component_block_id
 * @property-read \App\Models\Core\IT\Status\StatusComponentBlock $block
 * @property-read StatusComponent|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent whereStatusComponentBlockId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponent whereTitle($value)
 * @mixin \Eloquent
 */
final class StatusComponent extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function block()
    {
        return $this->belongsTo(StatusComponentBlock::class, 'status_component_block_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }
}
