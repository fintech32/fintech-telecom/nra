<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusMaintenance
 *
 * @property int $id
 * @property string $title
 * @property string $impact
 * @property string $status
 * @property \Illuminate\Support\Carbon $scheduled_for
 * @property \Illuminate\Support\Carbon $scheduled_until
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status_component_id
 * @property-read \App\Models\Core\IT\Status\StatusComponent|null $component
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereImpact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereScheduledFor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereScheduledUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereStatusComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class StatusMaintenance extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'scheduled_for', 'scheduled_until'];

    public function component()
    {
        return $this->belongsTo(StatusComponent::class);
    }
}
