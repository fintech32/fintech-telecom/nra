<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusSector
 *
 * @property int $id
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector whereTitle($value)
 * @mixin \Eloquent
 * @property string $status
 * @method static \Illuminate\Database\Eloquent\Builder|StatusSector whereStatus($value)
 */
final class StatusSector extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
