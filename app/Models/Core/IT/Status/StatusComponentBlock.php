<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusComponentBlock
 *
 * @property int $id
 * @property string $title
 * @property int $status_sector_id
 * @property-read \App\Models\Core\IT\Status\StatusSector|null $sector
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock whereStatusSectorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusComponentBlock whereTitle($value)
 * @mixin \Eloquent
 */
final class StatusComponentBlock extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function sector()
    {
        return $this->belongsTo(StatusSector::class);
    }
}
