<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Status;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Status\StatusMaintenanceAdvance
 *
 * @property int $id
 * @property string $status
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status_maintenance_id
 * @property-read \App\Models\Core\IT\Status\StatusMaintenance|null $maintenance
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance query()
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereStatusMaintenanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StatusMaintenanceAdvance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class StatusMaintenanceAdvance extends Model
{
    protected $guarded = [];

    public function maintenance()
    {
        return $this->belongsTo(StatusMaintenance::class);
    }
}
