<?php

declare(strict_types=1);

namespace App\Models\Core\IT\Helpdesk;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\IT\Helpdesk\HelpdeskCategory
 *
 * @property int $id
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|HelpdeskCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HelpdeskCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HelpdeskCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|HelpdeskCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HelpdeskCategory whereTitle($value)
 * @mixin \Eloquent
 */
final class HelpdeskCategory extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
