<?php

declare(strict_types=1);

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\ServiceCaracteristiquePrice
 *
 * @property int $id
 * @property float $price
 * @property int $caracteristique_id
 * @property-read \App\Models\Core\ServiceCaracteristique $caracteristique
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice whereCaracteristiqueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceCaracteristiquePrice wherePrice($value)
 * @mixin \Eloquent
 */
final class ServiceCaracteristiquePrice extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function caracteristique()
    {
        return $this->belongsTo(ServiceCaracteristique::class, 'caracteristique_id');
    }
}
