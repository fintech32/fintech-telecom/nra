<?php

declare(strict_types=1);

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\Service
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $low_price
 * @property int $active
 * @property int $new
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $subcategory_id
 * @property-read \App\Models\Core\Subcategory $subcategory
 * @method static \Illuminate\Database\Eloquent\Builder|Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereLowPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereSubcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $qte Certains service notamment les serveurs sont en quantité limité
 * @method static \Illuminate\Database\Eloquent\Builder|Service whereQte($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Core\ServiceCaracteristique[] $features
 * @property-read int|null $features_count
 */
final class Service extends Model
{
    protected $guarded = [];

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function features()
    {
        return $this->hasMany(ServiceCaracteristique::class);
    }
}
