<?php

declare(strict_types=1);

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Crm\CustomerContract
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property \Illuminate\Support\Carbon $date_obsolete
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property-read \App\Models\Crm\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereDateObsolete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerContract whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class CustomerContract extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'date_obsolete'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
