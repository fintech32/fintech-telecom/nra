<?php

declare(strict_types=1);

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Crm\CustomerMessage
 *
 * @property int $id
 * @property string $sujet
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property-read \App\Models\Crm\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereSujet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class CustomerMessage extends Model
{
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
