<?php

declare(strict_types=1);

namespace App\Models\Crm;

use App\Models\Core\IT\NiveauSupportSla;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Crm\Customer
 *
 * @property int $id
 * @property string $code_client 9999-9999-999
 * @property string $country 2 letter
 * @property string $type_compte
 * @property string $nom
 * @property string $prenom
 * @property string $email
 * @property string $adresse
 * @property string $postal
 * @property string $ville
 * @property string $portable
 * @property int $niveau_support_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property-read NiveauSupportSla $support
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereAdresse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCodeClient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereNiveauSupportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePortable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePrenom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereTypeCompte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereVille($value)
 * @mixin \Eloquent
 * @property string|null $stripe_customer_id
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereStripeCustomerId($value)
 */
final class Customer extends Model
{
    use Notifiable;
    protected $guarded = [];

    public function support()
    {
        return $this->belongsTo(NiveauSupportSla::class, 'niveau_support_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
