<?php

declare(strict_types=1);

namespace App\Models\Crm;

use App\Models\Core\Service;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Crm\CustomerService
 *
 * @property int $id
 * @property string $code_service
 * @property string $route Route vers le service dans l'espace client
 * @property string $name
 * @property string $status
 * @property int $service_id
 * @property int $customer_id
 * @property-read \App\Models\Crm\Customer $customer
 * @property-read Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereCodeService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereStatus($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon $date_creation
 * @property \Illuminate\Support\Carbon|null $date_engagement
 * @property \Illuminate\Support\Carbon|null $date_expiration
 * @property \Illuminate\Support\Carbon|null $date_next_billing
 * @property int $qte
 * @property int $renew_dayOfMonth
 * @property string $renew_interval
 * @property string $renew_mode
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereDateCreation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereDateEngagement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereDateExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereDateNextBilling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereQte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereRenewDayOfMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereRenewInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerService whereRenewMode($value)
 */
final class CustomerService extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['date_creation', 'date_engagement', 'date_expiration', 'date_next_billing'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
