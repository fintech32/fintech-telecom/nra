<?php

declare(strict_types=1);

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Crm\CustomerPaymentMethod
 *
 * @property int $id
 * @property string $type
 * @property string $merchand_id Identifiant du mode de paiement enregistrer
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $customer_id
 * @property-read \App\Models\Crm\Customer $customer
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereMerchandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomerPaymentMethod whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class CustomerPaymentMethod extends Model
{
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
