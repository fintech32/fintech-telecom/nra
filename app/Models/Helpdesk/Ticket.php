<?php

declare(strict_types=1);

namespace App\Models\Helpdesk;

use App\Models\Core\IT\Helpdesk\HelpdeskCategory;
use App\Models\Core\Service;
use App\Models\Crm\CustomerService;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Helpdesk\Ticket
 *
 * @property int $id
 * @property string $subject
 * @property string $last_message_from
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $helpdesk_category_id
 * @property int $customer_service_id
 * @property int $service_id
 * @property-read HelpdeskCategory $category
 * @property-read CustomerService $customer
 * @property-read Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCustomerServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereHelpdeskCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereLastMessageFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class Ticket extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(HelpdeskCategory::class);
    }

    public function customer()
    {
        return $this->belongsTo(CustomerService::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
