<?php

declare(strict_types=1);

namespace App\Service;

use Illuminate\Http\Request;
use Http;

final class Registrar
{
    public static function listDomains()
    {
        return Http::get('http://registrar.fintech.test/api/domain')->object();
    }

    public static function storeDomain(Request $request)
    {
        return Http::post('http://registrar.fintech.test/api/domain', [
            "domain" => $request->get('domain'),
            "customer_nom" => $request->get('customer_nom'),
            "customer_prenom" => $request->get('customer_prenom'),
            "customer_societe" => $request->get('customer_societe'),
            "customer_reference" => auth()->user()->customer->code_client,
        ])->object();
    }
}
