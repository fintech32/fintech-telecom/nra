<?php

declare(strict_types=1);

namespace App\Service\Proxmox;

use Proxmox\ProxmoxException;
use Proxmox\Request;

final class Proxmox
{
    public function login()
    {
        try {
            Request::Login([
                'hostname' => config('proxmox.proxmox_host'),
                'username' => config('proxmox.proxmox_username'),
                'token_name' => config('proxmox.proxmox_token_name'),
                'token_value' => config('proxmox.proxmox_token_secret'),
            ]);
        } catch (ProxmoxException $e) {
            return $e->getMessage();
        }
    }
}
