<?php

declare(strict_types=1);

namespace App\Service\Proxmox;

use Proxmox\Nodes;

final class Node extends Proxmox
{
    public function nodes()
    {
        $this->login();
        $d = new Nodes();
        return $d->listNodes();
    }
}
