<?php

declare(strict_types=1);

namespace App\Service\Proxmox;

final class Cluster extends Proxmox
{
    public function clusters()
    {
        $this->login();
        return \Proxmox\Cluster::Cluster();
    }
}
