<?php

declare(strict_types=1);

namespace App\Service;

use Docker\Docker;
use Docker\DockerClientFactory;

final class DockerApi
{
    public Docker $docker;

    public function __construct()
    {
        $client = DockerClientFactory::create([
            "ssl" => false
        ]);

        $this->docker = Docker::create($client);
    }
}
