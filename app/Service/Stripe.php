<?php

declare(strict_types=1);

namespace App\Service;

use Stripe\StripeClient;

final class Stripe
{
    public StripeClient $client;

    public function __construct()
    {
        $this->client = new StripeClient(config('services.stripe.api_secret'));
    }
}
