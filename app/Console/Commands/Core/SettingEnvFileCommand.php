<?php

declare(strict_types=1);

namespace App\Console\Commands\Core;

use Illuminate\Console\Command;
use MadWeb\Initializer\Contracts\Runner;

final class SettingEnvFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:setenv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setting Env File';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Runner $run)
    {
        $app_name = $this->ask('Quel est le nom de votre application ?', 'Laravel');
        $app_env = $this->choice("Quel est l'environnement ?", ['local', 'testing', 'production']);
        $app_url = $this->ask("Quel est l'adresse d'accès au site internet", 'https://localhost');

        if ('production' === $app_env) {
            $db_host = $this->ask('Mysql Host');
            $db_base = $this->ask('Mysql Database');
            $db_username = $this->ask('Mysql Username');
            $db_password = $this->ask('Mysql Password');

            $this->call("env:set db_host {$db_host}");
            $this->call("env:set db_base {$db_base}");
            $this->call("env:set db_username {$db_username}");
            $this->call("env:set db_password {$db_password}");
        }

        if ($this->confirm('Voulez-vous utiliser Stripe ?', true)) {
            $this->info('Veuillez vous connecter à votre Tableau de bord stripe et récuperer vos informations developpeur');
            $stripe_key = $this->ask('Cle Api');
            $stripe_secret = $this->ask('Cle Secret');
            $stripe_webhook = $this->ask('Cle Webhook');

            $this->call("env:set app_name {$app_name}");
            $this->call("env:set app_env {$app_env}");
            $this->call("env:set app_url {$app_url}");
            $this->call("env:set stripe_api_key {$stripe_key}");
            $this->call("env:set stripe_api_secret {$stripe_secret}");
            $this->call("env:set stripe_api_webhook {$stripe_webhook}");
        } else {
            $this->call("env:set app_name {$app_name}");
            $this->call("env:set app_env {$app_env}");
            $this->call("env:set app_url {$app_url}");
        }

        return 0;
    }
}
