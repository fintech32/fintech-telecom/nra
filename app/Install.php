<?php

declare(strict_types=1);

namespace App;

use Illuminate\Console\Command;
use MadWeb\Initializer\Contracts\Runner;

final class Install extends Command
{
    public function production(Runner $run): void
    {
        $run->artisan('key:generate')
            ->external('rm', '-rf', '.env')
            ->external('cp', '.env.production', '.env')
            ->artisan('storage:link')
            ->external('npm', 'install', '--production')
            ->external('npm', 'run', 'production');
    }

    public function local(Runner $run): void
    {
        $run->artisan('key:generate')
            ->external('rm', '-rf', '.env')
            ->external('cp', '.env.example', '.env')
            ->artisan('log-viewer:publish')
            ->artisan('storage:link')
            ->external('npm', 'install')
            ->external('npm', 'run', 'development')
            ->external('php', 'vendor/bin/homestead', 'make')
            ->external('git', 'init', '--initial-branch=develop')
            ->external('git', 'flow', 'init');
    }
}
