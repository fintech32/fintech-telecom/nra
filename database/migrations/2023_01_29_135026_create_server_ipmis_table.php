<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_ipmis', function (Blueprint $table): void {
            $table->id();
            $table->boolean('activated')->default(false);
            $table->boolean('applet_java')->default(false);
            $table->boolean('nav_serial_lan_over')->default(false);
            $table->boolean('kvm')->default(false);

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_ipmis');
    }
};
