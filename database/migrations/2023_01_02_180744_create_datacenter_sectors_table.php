<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('datacenter_sectors', function (Blueprint $table): void {
            $table->id();
            $table->boolean('dedicated_server')->default(false);
            $table->boolean('dedicated_server_eco')->default(false);
            $table->boolean('vps')->default(false);
            $table->boolean('nas_ha')->default(false);
            $table->boolean('cloud_file')->default(false);
            $table->boolean('ip_address')->default(false);
            $table->boolean('load_balancer')->default(false);
            $table->boolean('vrack')->default(false);
            $table->boolean('ssl_gateway')->default(false);
            $table->boolean('hebergement')->default(false);
            $table->boolean('domain')->default(false);
            $table->boolean('cloud_db')->default(false);
            $table->boolean('email')->default(false);
            $table->boolean('email_pro')->default(false);
            $table->boolean('collaboration')->default(false);
            $table->boolean('office')->default(false);
            $table->boolean('dns_hosting')->default(false);
            $table->boolean('dns_anycast')->default(false);
            $table->boolean('cdn')->default(false);
            $table->boolean('voip_sip')->default(false);
            $table->boolean('voip_interconnect')->default(false);
            $table->boolean('voip_telephony')->default(false);
            $table->boolean('internet_adsl')->default(false);
            $table->boolean('internet_ftth')->default(false);

            $table->foreignId('datacenter_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('datacenter_sectors');
    }
};
