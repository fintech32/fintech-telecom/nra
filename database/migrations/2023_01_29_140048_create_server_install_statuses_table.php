<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_install_statuses', function (Blueprint $table): void {
            $table->id();
            $table->text('comment');
            $table->text('error')->nullable();
            $table->enum('status', [
                "doing",
                "done",
                "error",
                "expired",
                "idle",
                "pending",
                "stopping",
                "todo",
            ])->default('todo');
            $table->timestamps();

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_install_statuses');
    }
};
