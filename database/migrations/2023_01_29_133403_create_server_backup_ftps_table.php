<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_backup_ftps', function (Blueprint $table): void {
            $table->id();
            $table->string('name')->nullable();
            $table->bigInteger('quota')->comment("en Octets");
            $table->enum('type', ["included", "storage"]);
            $table->bigInteger('used')->default(0)->comment("En octets");

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_backup_ftps');
    }
};
