<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('customers', function (Blueprint $table): void {
            $table->id();
            $table->string('code_client')->comment('9999-9999-999');
            $table->string('country')->comment("2 letter");
            $table->enum('type_compte', [
                "part",
                "admin",
                "assoc",
                "pro",
                "other"
            ])->default('part');
            $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->string('adresse');
            $table->string('postal');
            $table->string('ville');
            $table->string('portable');
            $table->bigInteger('niveau_support_id')->unsigned();
            $table->string('stripe_customer_id')->nullable();
            $table->timestamps();

            $table->foreign('niveau_support_id')
                ->on('niveau_support_slas')
                ->references('id')
                ->cascadeOnUpdate();

            $table->foreignId('user_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('customers');
    }
};
