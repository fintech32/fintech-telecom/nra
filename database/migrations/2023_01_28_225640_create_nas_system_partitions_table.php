<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('nas_system_partitions', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->enum('protocol', [
                "CIFS",
                "NFS",
                "NFS_CIFS",
            ])->default('NFS');
            $table->bigInteger('size')->comment("En Octets");

            $table->foreignId('nas_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('nas_system_partitions');
    }
};
