<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

final class AddCartSessionIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if ( ! (Schema::hasColumn(config('laracart.database.table'), 'cart_session_id'))) {
            Schema::table(config('laracart.database.table'), function (Blueprint $table): void {
                $table->string('cart_session_id')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        if ((Schema::hasColumn(config('laracart.database.table'), 'cart_session_id'))) {
            Schema::table(config('laracart.database.table'), function (Blueprint $table): void {
                $table->dropColumn('cart_session_id');
            });
        }
    }
}
