<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_components', function (Blueprint $table): void {
            $table->id();
            $table->string('title');
            $table->enum('status', [
                "operational", // Systeme Opérationnel
                "degraded_performance", // Systeme dégrader Zabbix informe que certains systeme sont lent
                "partial_outage", // Systeme Avec panne partiel: Zabbix ne peut ce connecter à certain systeme
                "major_outage", // Systeme avec panne majeur: Zabbix informe que certain systeme sont en phase critique / Déclenche une alerte evenement (Status_EVENT)
                "maintenance" // Systeme en maintenance programmée
            ])->default('operational');

            $table->bigInteger('parent_id')->unsigned()->nullable();

            $table->foreign('parent_id')
                ->on('status_components')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('status_component_block_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_components');
    }
};
