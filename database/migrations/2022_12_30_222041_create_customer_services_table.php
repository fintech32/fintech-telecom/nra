<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('customer_services', function (Blueprint $table): void {
            $table->id();
            $table->string('code_service');
            $table->timestamp('date_creation');
            $table->timestamp('date_engagement')->nullable();
            $table->timestamp('date_expiration')->nullable();
            $table->timestamp('date_next_billing')->nullable();
            $table->integer('qte');
            $table->integer('renew_dayOfMonth')->default(1);
            $table->enum('renew_interval', [
                "P1M", // Mensuel
                "P3M", // Trimestriel
                "P6M", // Semestriel
                "P1Y", // Annuel
                "P2Y", // 2 Ans
                "P3Y" // 3 Ans
            ])->default("P1M");
            $table->enum("renew_mode", [
                "auto",
                "deleteAtExpiration",
                "deleteAtEngagement",
                "manual",
                "oneshot",
            ])->default('auto');
            $table->string('route')->nullable();
            $table->enum('status', [
                "expired", // Service expiré
                "ok", // Service OK
                "pending", // Service en attente
                "unpaid", // Service non réglé
            ])->default('ok');

            $table->foreignId('service_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('customer_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('customer_services');
    }
};
