<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_ipmi_accesses', function (Blueprint $table): void {
            $table->id();
            $table->ipAddress('ipToAllow')->default('*');
            $table->text('sshKey');
            $table->enum('ttl', ['1', '10', '15', '3', '5']);
            $table->enum('type', ['applet_java', 'nav_serial_lan_over', 'kvm']);

            $table->foreignId('server_ipmi_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_ipmi_accesses');
    }
};
