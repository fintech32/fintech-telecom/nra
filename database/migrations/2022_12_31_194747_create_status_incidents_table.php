<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_incidents', function (Blueprint $table): void {
            $table->id();
            $table->enum('step', [
                'investigating', // Recherche du problème
                'identified', // Problème identifié et en cours de réglage
                'monitoring', // Monitoring de la solution apporté sur une durée de 1 Heure
                'resolved' // Problème résolue
            ]);
            $table->longText('body');
            $table->timestamps();

            $table->foreignId('status_event_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_incidents');
    }
};
