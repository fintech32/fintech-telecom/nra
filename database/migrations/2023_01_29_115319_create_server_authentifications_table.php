<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_authentifications', function (Blueprint $table): void {
            $table->id();
            $table->string('password');
            $table->enum('type', [
                "DIRECTADMIN_PANEL",
                "PLESK_PANEL",
                "PROXMOX_PANEL",
                "RDP",
                "SQL_SERVER",
                "SSH",
                "WEB_PANEL",
            ])->default('SSH');
            $table->string('url')->nullable();
            $table->string('user');

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_authentifications');
    }
};
