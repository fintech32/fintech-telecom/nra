<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_maintenances', function (Blueprint $table): void {
            $table->id();
            $table->string('title');
            $table->enum('impact', [
                'none',
                'minor',
                'major',
                'critical'
            ])->default('none');
            $table->enum('status', [
                'scheduled',
                "progress",
                "verifying",
                "completed"
            ]);
            $table->timestamp('scheduled_for');
            $table->timestamp('scheduled_until');
            $table->timestamps();

            $table->foreignId('status_component_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_maintenances');
    }
};
