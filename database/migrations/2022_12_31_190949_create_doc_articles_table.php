<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('doc_articles', function (Blueprint $table): void {
            $table->id();
            $table->string('code_article')->comment("Format: KB999999999");
            $table->longText('objectif');
            $table->longText('prerequis');
            $table->longText('pratique');
            $table->integer('count_view')->default(0);
            $table->integer('article_util')->default(0);
            $table->integer('article_non_util')->default(0);
            $table->boolean('active')->default(false);
            $table->timestamps();

            $table->foreignId('doc_sub_category_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('doc_articles');
    }
};
