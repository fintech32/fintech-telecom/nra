<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('service_caracteristiques', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->string('value');
            $table->string('type')->comment("Par example: memory pour memoire");

            $table->foreignId('service_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('service_caracteristiques');
    }
};
