<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('invoice_payments', function (Blueprint $table): void {
            $table->id();
            $table->timestamp('paymentDate');
            $table->enum('paymentType', [
                "creditAccount",
                "creditCard",
                "fidelityPoint",
                "free",
                "walletAccount",
                "none",
                "mandat",
                "transfer",
                "refund"
            ]);
            $table->string('paymentIdentifier')->nullable();

            $table->foreignId('customer_payment_method_id')
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('invoice_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('invoice_payments');
    }
};
