<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('customer_contracts', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->enum('status', [
                "ko",
                "ok",
                "todo",
                "obsolete"
            ])->default('todo');

            $table->timestamp('date_obsolete')->default(now()->addMonth());
            $table->timestamps();

            $table->foreignId('customer_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('customer_contracts');
    }
};
