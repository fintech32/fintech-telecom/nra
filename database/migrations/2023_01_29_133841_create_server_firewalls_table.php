<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_firewalls', function (Blueprint $table): void {
            $table->id();
            $table->string('description')->nullable();
            $table->boolean('enabled')->default(false);
            $table->ipAddress();
            $table->enum('mode', [
                "routed",
                "transparent"
            ])->default('routed');

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_firewalls');
    }
};
