<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('nas_systems', function (Blueprint $table): void {
            $table->id();
            $table->string('customName')->nullable();
            $table->ipAddress();
            $table->string('mountPath');
            $table->bigInteger('size')->comment("En Octets");

            $table->timestamps();

            $table->foreignId('customer_service_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('datacenter_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('nas_systems');
    }
};
