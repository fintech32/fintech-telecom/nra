<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('service_caracteristique_prices', function (Blueprint $table): void {
            $table->id();
            $table->float('price')->default(0);
            $table->bigInteger('caracteristique_id')->unsigned();

            $table->foreign('caracteristique_id')
                ->on('service_caracteristiques')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('service_caracteristique_prices');
    }
};
