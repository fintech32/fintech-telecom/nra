<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_firewall_outgoings', function (Blueprint $table): void {
            $table->id();
            $table->string('name')->nullable();
            $table->enum('versionIP', ["IPV4", "IPV6"]);
            $table->enum("protocol", ["TCP", "UDP"]);
            $table->integer('port');
            $table->boolean('active');

            $table->foreignId('server_firewall_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_firewall_outgoings');
    }
};
