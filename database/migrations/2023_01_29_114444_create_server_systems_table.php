<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_systems', function (Blueprint $table): void {
            $table->id();
            $table->ipAddress();
            $table->boolean('monitoring')->default(true);
            $table->boolean('professionalUse');
            $table->string('hostname');
            $table->string('os_system');
            $table->enum('power_state', ['poweron', 'poweroff'])->default('poweroff');
            $table->string('rescueMail')->nullable();
            $table->string('reverse_dns')->nullable();
            $table->enum('state', [
                "error",
                "hacked",
                "hackedBlocked",
                "ok"
            ])->default('ok');


            $table->foreignId('datacenter_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('customer_service_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_systems');
    }
};
