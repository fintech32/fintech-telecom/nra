<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_events', function (Blueprint $table): void {
            $table->id();
            $table->string('title');
            $table->enum('impact', [
                'none',
                'minor',
                'major',
                'critical'
            ])->default('none');
            $table->enum('status', [
                'investigating', // Recherche du problème
                'identified', // Problème identifié et en cours de réglage
                'monitoring', // Monitoring de la solution apporté sur une durée de 1 Heure
                'resolved' // Problème résolue
            ]);
            $table->timestamp('monitoring_at')->nullable();
            $table->timestamps();

            $table->foreignId('status_component_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_events');
    }
};
