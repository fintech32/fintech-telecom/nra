<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('server_boots', function (Blueprint $table): void {
            $table->id();
            $table->enum("type", [
                "harddisk",
                "internal",
                "ipxeCustomerScript",
                "network",
                "power",
                "rescue",
            ])->default('harddisk');
            $table->string('description')->nullable();
            $table->string('kernel')->nullable();

            $table->foreignId('server_system_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('server_boots');
    }
};
