<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('niveau_support_slas', function (Blueprint $table): void {
            $table->id();
            $table->string('name');
            $table->float('price')->default(0);
            $table->integer('hour_ticket_relevance');
            $table->integer('hour_reso_relevance');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('niveau_support_slas');
    }
};
