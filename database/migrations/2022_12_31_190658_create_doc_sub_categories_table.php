<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('doc_sub_categories', function (Blueprint $table): void {
            $table->id();
            $table->string('title');
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreignId('doc_category_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('parent_id')
                ->on('doc_sub_categories')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('doc_sub_categories');
    }
};
