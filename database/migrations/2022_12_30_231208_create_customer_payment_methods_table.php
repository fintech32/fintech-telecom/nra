<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('customer_payment_methods', function (Blueprint $table): void {
            $table->id();
            $table->enum('type', [
                "card", // Stripe
                "prlv", // GoCardless
                "paypal" // Paypal
            ]);
            $table->string('merchand_id')->comment("Identifiant du mode de paiement enregistrer");
            $table->enum('status', [
                "CANCELED", // Annulée
                "CANCELING", // Annulation en cours
                "CREATED", // Créer
                "CREATING", // Création en cours
                "ERROR", // Erreur
                "EXPIRED", // Expirer
                "FAILED", // Echouer
                "MAINTENANCE", // En maintenance (Vérifier l'état du provider toutes les heures)
                "REJECTED", // Rejeter (Uniquement pour prlv)
                "VALID", // Ok
                "VALIDATING", // Vérification en cours
            ])->default('CREATING');
            $table->timestamps();

            $table->foreignId('customer_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('customer_payment_methods');
    }
};
