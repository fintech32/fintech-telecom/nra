<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('invoices', function (Blueprint $table): void {
            $table->id();
            $table->string('invoice_number')->comment("Format: FR-YYYY-999999");
            $table->float('price_ht');
            $table->float('price_tva');
            $table->float('price_ttc');
            $table->enum('status', [
                "creating", // Création de la facture
                "created", // Facture créer et envoie au client
                "paid", // Facture Payer
                "unpaid", // Facture non payer
            ]);
            $table->timestamps();

            $table->foreignId('order_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('invoices');
    }
};
