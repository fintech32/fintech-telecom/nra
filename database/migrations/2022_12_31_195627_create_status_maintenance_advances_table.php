<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('status_maintenance_advances', function (Blueprint $table): void {
            $table->id();
            $table->enum('status', [
                'scheduled',
                "progress",
                "verifying",
                "completed"
            ]);
            $table->longText('body');
            $table->timestamps();

            $table->foreignId('status_maintenance_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('status_maintenance_advances');
    }
};
