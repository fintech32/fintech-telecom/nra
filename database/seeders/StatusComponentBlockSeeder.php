<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\IT\Status\StatusComponentBlock;
use Illuminate\Database\Seeder;

final class StatusComponentBlockSeeder extends Seeder
{
    public function run(): void
    {
        StatusComponentBlock::create(["title" => "Serveurs Dédié", "status_sector_id" => 1]);
        StatusComponentBlock::create(["title" => "Serveurs Virtuel", "status_sector_id" => 1]);
        StatusComponentBlock::create(["title" => "Stockage & Sauvegarde", "status_sector_id" => 1]);
        StatusComponentBlock::create(["title" => "Service réseau", "status_sector_id" => 1]);
        StatusComponentBlock::create(["title" => "Service DNS", "status_sector_id" => 1]);

        StatusComponentBlock::create(["title" => "Hebergement Web", "status_sector_id" => 2]);
        StatusComponentBlock::create(["title" => "Domaine", "status_sector_id" => 2]);
        StatusComponentBlock::create(["title" => "Base de données", "status_sector_id" => 2]);
        StatusComponentBlock::create(["title" => "CDN", "status_sector_id" => 2]);
        StatusComponentBlock::create(["title" => "DNS", "status_sector_id" => 2]);
        StatusComponentBlock::create(["title" => "EMAIL", "status_sector_id" => 2]);

        StatusComponentBlock::create(["title" => "VOIP", "status_sector_id" => 3]);
        StatusComponentBlock::create(["title" => "Accès Internet", "status_sector_id" => 3]);
        StatusComponentBlock::create(["title" => "Accès Réseau Mobile", "status_sector_id" => 3]);

        StatusComponentBlock::create(["title" => "Infrastructure", "status_sector_id" => 4]);
        StatusComponentBlock::create(["title" => "Service réseau", "status_sector_id" => 4]);

        StatusComponentBlock::create(["title" => "Support", "status_sector_id" => 5]);
        StatusComponentBlock::create(["title" => "Espace client", "status_sector_id" => 5]);
        StatusComponentBlock::create(["title" => "Site Internet", "status_sector_id" => 5]);
    }
}
