<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\IT\Parc\Datacenter;
use Illuminate\Database\Seeder;

final class DatacenterSeeder extends Seeder
{
    public function run(): void
    {
        /**
         * Paris: Hebergement Web / Serveur Dédié / DNS ANYCAST
         * Marseille: Hebergement Web / CloudDB / DNS ANYCAST
         * Brest: Hebergement Web / VPS
         * Toulouse: Serveur Dédié / VPS
         * Bordeaux: Serveur Dédié / NAS HA / backup Storage
         * Strasbourg: Serveur Dédié / NAS HA / backup Storage
         * Roubaix: CloudDB / VPS / Entreprise Storage
         */
        $cities = ["Paris", "Marseille", "Brest", "Toulouse", "Bordeaux", "Strasbourg", "Roubaix"];

        Datacenter::create([
            "name" => "PAR",
            "city" => "Paris",
            "primary_ip" => "172.18.0.2"
        ]);

        Datacenter::create([
            "name" => "MAR",
            "city" => "Marseille",
            "primary_ip" => "172.19.0.2"
        ]);

        Datacenter::create([
            "name" => "BRS",
            "city" => "Brest",
            "primary_ip" => "172.20.0.2"
        ]);

        Datacenter::create([
            "name" => "TLS",
            "city" => "Toulouse",
            "primary_ip" => "172.21.0.2"
        ]);

        Datacenter::create([
            "name" => "BRX",
            "city" => "Bordeaux",
            "primary_ip" => "172.21.0.2"
        ]);

        Datacenter::create([
            "name" => "STR",
            "city" => "Strasbourg",
            "primary_ip" => "172.22.0.2"
        ]);

        Datacenter::create([
            "name" => "RBX",
            "city" => "Roubaix",
            "primary_ip" => "172.23.0.2"
        ]);
    }
}
