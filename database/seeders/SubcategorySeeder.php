<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\Subcategory;
use Illuminate\Database\Seeder;

final class SubcategorySeeder extends Seeder
{
    public function run(): void
    {
        Subcategory::create([
            "name" => "Serveurs Dédié",
            "description" => "Avec ".config('app.name').", appuyez-vous sur un expert du bare metal. Hébergez votre site web, déployez votre infrastructure à haute résilience ou personnalisez votre machine pour l’adapter à vos projets, en quelques clics.",
            "category_id" => 1
        ])->create([
            "name" => "Serveurs Dédié ECO",
            "description" => "Choisissez, parmi nos gammes Economique, le serveur dédié le plus adapté à votre projet.",
            "category_id" => 1
        ])->create([
            "name" => "Serveurs Privés Virtuels (VPS)",
            "description" => "Simple et rapide – votre environnement cloud au meilleur prix",
            "category_id" => 1
        ])->create([
            "name" => "Stockages & Sauvegarde",
            "description" => "Les données que vous stockez et leurs différentes utilisations ont des contraintes spécifiques et nécessitent une solution adaptée. ".config('app.name')." propose des solutions de stockage cloud pour répondre à vos besoins : stockage d’objets, par bloc ou de fichiers, espace d’archivage longue durée ou solution de sauvegarde pour tous vos services.",
            "category_id" => 1
        ])->create([
            "name" => "Réseau",
            "description" => "Vos applications et leurs composants doivent être accessibles 24 h/24 7 j/7, toute l’année. Qu'il s'agisse d'une connectivité privée, publique ou on-premises, nos solutions vous aident à répondre à vos besoins en matière de réseau. Quant à nos réseaux privés, ils assurent une communication sécurisée entre vos serveurs et vos services. ".config('app.name')." opère également sa propre backbone. La backbone ".config('app.name')." est conçue pour assurer un accès rapide et fiable à vos applications, partout et à tout moment. Vous gardez le contrôle total de vos données. Aucune exception.",
            "category_id" => 1
        ])->create([
            "name" => "Sécurité réseau",
            "description" => "Les cyberattaques mettent en danger votre infrastructure. Elles peuvent causer des défaillances et perturber vos services. Les solutions sécurité réseau ".config('app.name')." vous permettent d'éliminer ces risques et de vous concentrer sur l'essentiel : votre activité. ".config('app.name')." vous propose plusieurs niveaux de protection. La protection de l'infrastructure garantit la sécurité de cette dernière contre les activités malveillantes. La sécurité des hôtes et services offre une protection supplémentaire pour vos applications, les jeux par exemple. La sécurité web assure quant à elle un fonctionnement optimal de vos services web. Grâce à sa structure distribuée et à notre réseau mondial, le système de sécurité de ".config('app.name')." a permis de mitiger certaines des plus grandes cyberattaques connues.",
            "category_id" => 1
        ]);

        Subcategory::create([
            "name" => "Nom de domaine",
            "description" => "Création et transfert de noms de domaine",
            "category_id" => 2
        ])->create([
            "name" => "Hébergements Web",
            "description" => "Créez votre premier site web en quelques minutes. Publiez votre blog, votre site internet professionnel ou encore votre boutique en ligne.",
            "category_id" => 2
        ])->create([
            "name" => "Solution E-mail",
            "description" => "Une gamme complète de messageries professionnelles hébergées en France adaptée à tous vos besoins de messageries en ligne.",
            "category_id" => 2
        ])->create([
            "name" => "Certificats SSL",
            "description" => "Votre hébergement web inclut par défaut et gratuitement un certificat SSL Let's Encrypt afin de sécuriser votre site Internet. Pour bénéficier d'un niveau de certification plus élevé et de garanties financières, optez pour les certificats SSL Sectigo. Ces derniers sont essentiels pour les sites e-commerce ou les sites web d’entreprise. Le choix du niveau de certificats SSL souhaité s'effectue lors de la commande de votre hébergement web ou depuis votre espace client.",
            "category_id" => 2
        ])->create([
            "name" => "CDN",
            "description" => "Le CDN met en cache les contenus statiques et les distribue en un temps record à vos visiteurs, où qu'ils se situent dans le monde et sans solliciter vos serveurs d'hébergement. Les pages de votre site Internet chargent alors bien plus rapidement, rendant l’expérience visiteur beaucoup plus agréable. Votre site web est également mieux référencé naturellement sur les moteurs de recherche.",
            "category_id" => 2
        ])->create([
            "name" => "Bases de données Start SQL",
            "description" => "Vous avez la possibilité d’ajouter des bases de données  Start SQL MySQL supplémentaires à votre hébergement. Augmentez la puissance de votre hébergement ainsi que vos possibilités de stockage. Les bases de données MySQL permettent également une gestion optimisée de vos données. Cette solution est idéale pour booster vos hébergements Perso ou Pro.",
            "category_id" => 2
        ])->create([
            "name" => "Bases de données SQL Privé",
            "description" => "Vous avez également la possibilité d’opter pour des bases de données SQL privées. Vous avez le choix entre nos différentes solutions MySQL, MariaDB ou PostgreSQL. Elles vous apportent davantage de liberté de configuration au sein de votre hébergement.",
            "category_id" => 2
        ])->create([
            "name" => "Option BOOST",
            "description" => "Cette option s’adresse aux titulaires d’un hébergement Performance et vous permet de faire face à des pics temporaires de trafics ou toute autre activité entraînant une consommation de ressources élevée. Elle vous permet de passer au niveau d’offre Performance supérieur de façon temporaire. Activable en un clic depuis votre espace client, l’Option Boost permet de gérer les situations exceptionnelles pour votre activité.",
            "category_id" => 2
        ]);

        Subcategory::create([
            "name" => "Fibre & Accès haut débit",
            "description" => "Connexion fluide, besoins en téléphonie professionnelle, redondance d'accès, autoroute vers le Cloud ...",
            "category_id" => 3,
        ])->create([
            "name" => "Forfait VOIP",
            "description" => "La VoIP convertit la voix en signal numérique en s’appuyant sur la puissance d'Internet.<br>
Nos offres vous permettent de bénéficier de tarifs attractifs et de l’évolution en temps réel de notre parc. Apportez à vos utilisateurs une expérience optimale grâce au couplage téléphonie-informatique de ".config('app.name').".",
            "category_id" => 3,
        ])->create([
            "name" => "PACK SIP TRUNK",
            "description" => "Votre téléphonie se base sur un IPBX ?<br>Profitez d'un Trunk avec une consommation facturée à la seconde.",
            "category_id" => 3,
        ])->create([
            "name" => "SIP TRUNK AVEC FORFAIT D'APPELS",
            "description" => "Idéal pour un usage téléphonique classique, souscrivez un Trunk avec forfait d'appels fixe et mobile inclus. Votre budget est maitrisé !",
            "category_id" => 3,
        ])->create([
            "name" => "POSTE OPERATEUR PC",
            "description" => "Un accueil professionnel et efficace pour vos appels ! Le POPC (Poste Opérateur sur PC) simplifie et fluidifie la gestion de vos communications. Evitez en 1 clic les appels perdus et les transferts chaotiques !",
            "category_id" => 3,
        ])->create([
            "name" => "Numéros",
            "description" => "Numéros, la carte de visite de votre entreprise",
            "category_id" => 3,
        ])->create([
            "name" => "SMS",
            "description" => "L'envoi de SMS par Internet n'a jamais été aussi simple.",
            "category_id" => 3,
        ]);

        Subcategory::create([
            "name" => "Nos Forfaits",
            "description" => "Seulement 3 forfaits mobile",
            "category_id" => 4
        ])->create([
            "name" => "Nos Téléphones",
            "description" => null,
            "category_id" => 4
        ]);
    }
}
