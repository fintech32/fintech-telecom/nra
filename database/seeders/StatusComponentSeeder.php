<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\IT\Parc\Datacenter;
use App\Models\Core\IT\Status\StatusComponent;
use Illuminate\Database\Seeder;

final class StatusComponentSeeder extends Seeder
{
    public function run(): void
    {
        $this->baremetal();
        $this->webcloud();
        $this->telecom();
        $this->infrastructure();
        $this->serviceclient();

        $this->subbarmetaldedicatedserveur();
        $this->subbarmetalvps();
        $this->subbarstockage();
        $this->subbarreseau();
        $this->subbardns();

        $this->subwebssl();
        $this->subwebdomain();
        $this->subwebcdninfra();
        $this->subwebcdnhost();
        $this->subwebdnshost();
        $this->subwebdnsanycast();
        $this->subwebemail();
        $this->subwebemailpro();
        $this->subwebemailCollabor();
        $this->subwebemailoffice();
    }

    private function baremetal(): void
    {
        // Serveur Dédié
        StatusComponent::create([
            "title" => "Infrastructure Global",
            "status_component_block_id" => 1
        ])->create([
            "title" => "Livraison",
            "status_component_block_id" => 1
        ])->create([
            "title" => "Système Opérateur",
            "status_component_block_id" => 1
        ])->create([
            "title" => "Service additionnel",
            "status_component_block_id" => 1
        ])->create([
            "title" => "Réseau",
            "status_component_block_id" => 1
        ]);

        // Serveur Virtuel
        StatusComponent::create([
            "title" => "Infrastructure Global",
            "status_component_block_id" => 2
        ])->create([
            "title" => "Livraison",
            "status_component_block_id" => 2
        ])->create([
            "title" => "Système Opérateur",
            "status_component_block_id" => 2
        ])->create([
            "title" => "Service additionnel",
            "status_component_block_id" => 2
        ])->create([
            "title" => "Réseau",
            "status_component_block_id" => 2
        ]);

        // Stockage & Sauvegarde
        StatusComponent::create([
            "title" => "NAS HA",
            "status_component_block_id" => 3
        ])->create([
            "title" => "Backup Storage",
            "status_component_block_id" => 3
        ])->create([
            "title" => "Entreprise File Storage",
            "status_component_block_id" => 3
        ]);

        // Service Réseau
        StatusComponent::create([
            "title" => "Load Balancer",
            "status_component_block_id" => 4
        ]);

        // Service DNS
        StatusComponent::create([
            "title" => "DNS Secondaire",
            "status_component_block_id" => 5
        ])->create([
            "title" => "DNS Resolver",
            "status_component_block_id" => 5
        ]);
    }

    private function webcloud(): void
    {
        // Hebergement Weh
        StatusComponent::create([
            "title" => "Datacenter PAR",
            "status_component_block_id" => 6
        ])->create([
            "title" => "Datacenter MAR",
            "status_component_block_id" => 6
        ])->create([
            "title" => "Datacenter BRE",
            "status_component_block_id" => 6
        ])->create([
            "title" => "SSL",
            "status_component_block_id" => 6
        ]);

        // Domaine
        StatusComponent::create([
            "title" => "Nom de domaine",
            "status_component_block_id" => 7
        ]);

        // Base de données
        StatusComponent::create([
            "title" => "CloudDB MAR",
            "status_component_block_id" => 8
        ])->create([
            "title" => "CloudDB ROU",
            "status_component_block_id" => 8
        ]);

        // CDN
        StatusComponent::create([
            "title" => "CDN Infrastructure",
            "status_component_block_id" => 9
        ])->create([
            "title" => "CDN Hosting",
            "status_component_block_id" => 9
        ]);

        // DNS
        StatusComponent::create([
            "title" => "DNS Hosting",
            "status_component_block_id" => 10
        ])->create([
            "title" => "DNS Anycast",
            "status_component_block_id" => 10
        ]);

        // Email
        StatusComponent::create([
            "title" => "Email",
            "status_component_block_id" => 11
        ])->create([
            "title" => "Email Pro",
            "status_component_block_id" => 11
        ])->create([
            "title" => "Collabors",
            "status_component_block_id" => 11
        ])->create([
            "title" => "Virtual Office System",
            "status_component_block_id" => 11
        ]);
    }

    private function telecom(): void
    {
        // VOIP
        StatusComponent::create([
            "title" => "Core Network",
            "status_component_block_id" => 12
        ])->create([
            "title" => "Interconnect",
            "status_component_block_id" => 12
        ])->create([
            "title" => "Services",
            "status_component_block_id" => 12
        ])->create([
            "title" => "Telephonie",
            "status_component_block_id" => 12
        ]);

        // Accès Internet
        StatusComponent::create([
            "title" => "ADSL",
            "status_component_block_id" => 13
        ])->create([
            "title" => "FTTH",
            "status_component_block_id" => 13
        ])->create([
            "title" => "Provision & Livraison",
            "status_component_block_id" => 13
        ]);

        // Accès Réseau Mobile
        StatusComponent::create([
            "title" => "Infrastructure",
            "status_component_block_id" => 14
        ])->create([
            "title" => "Provision & Livraison",
            "status_component_block_id" => 14
        ]);
    }

    private function infrastructure(): void
    {
        // Infrastructure
        // Liste des datacentres
        foreach (Datacenter::all() as $datacenter) {
            StatusComponent::create([
                "title" => $datacenter->name,
                "status_component_block_id" => 15
            ]);
        }

        // Service Réseau
        StatusComponent::create([
            "title" => "vRack Private Network",
            "status_component_block_id" => 16
        ])->create([
            "title" => "Additional IP",
            "status_component_block_id" => 16
        ]);
    }

    private function serviceclient(): void
    {
        // Support
        StatusComponent::create([
            "title" => "France",
            "status_component_block_id" => 17
        ]);

        // Espace client
        StatusComponent::create([
            "title" => "Espace client",
            "status_component_block_id" => 18
        ])->create([
            "title" => "API",
            "status_component_block_id" => 18
        ]);

        // Site Web
        StatusComponent::create([
            "title" => "Site Internet",
            "status_component_block_id" => 19
        ]);
    }

    private function subbarmetaldedicatedserveur(): void
    {
        // Serveur dédié
        StatusComponent::create([
            "title" => "PAR",
            "parent_id" => 1,
            "status_component_block_id" => 1
        ])->create([
            "title" => "TOU",
            "parent_id" => 1,
            "status_component_block_id" => 1
        ])->create([
            "title" => "BOR",
            "parent_id" => 1,
            "status_component_block_id" => 1
        ])->create([
            "title" => "STR",
            "parent_id" => 1,
            "status_component_block_id" => 1
        ]);

        StatusComponent::create([
            "title" => "PAR",
            "parent_id" => 4,
            "status_component_block_id" => 1
        ])->create([
            "title" => "TOU",
            "parent_id" => 4,
            "status_component_block_id" => 1
        ])->create([
            "title" => "BOR",
            "parent_id" => 4,
            "status_component_block_id" => 1
        ])->create([
            "title" => "STR",
            "parent_id" => 4,
            "status_component_block_id" => 1
        ]);

        StatusComponent::create([
            "title" => "PAR",
            "parent_id" => 5,
            "status_component_block_id" => 1
        ])->create([
            "title" => "TOU",
            "parent_id" => 5,
            "status_component_block_id" => 1
        ])->create([
            "title" => "BOR",
            "parent_id" => 5,
            "status_component_block_id" => 1
        ])->create([
            "title" => "STR",
            "parent_id" => 5,
            "status_component_block_id" => 1
        ]);
    }

    private function subbarmetalvps(): void
    {
        StatusComponent::create([
            "title" => "BRE",
            "parent_id" => 6,
            "status_component_block_id" => 2
        ])->create([
            "title" => "TOU",
            "parent_id" => 6,
            "status_component_block_id" => 2
        ])->create([
            "title" => "RBX",
            "parent_id" => 6,
            "status_component_block_id" => 2
        ]);

        StatusComponent::create([
            "title" => "BRE",
            "parent_id" => 7,
            "status_component_block_id" => 2
        ])->create([
            "title" => "TOU",
            "parent_id" => 7,
            "status_component_block_id" => 2
        ])->create([
            "title" => "RBX",
            "parent_id" => 7,
            "status_component_block_id" => 2
        ]);

        StatusComponent::create([
            "title" => "BRE",
            "parent_id" => 9,
            "status_component_block_id" => 2
        ])->create([
            "title" => "TOU",
            "parent_id" => 9,
            "status_component_block_id" => 2
        ])->create([
            "title" => "RBX",
            "parent_id" => 9,
            "status_component_block_id" => 2
        ]);

        StatusComponent::create([
            "title" => "BRE",
            "parent_id" => 10,
            "status_component_block_id" => 2
        ])->create([
            "title" => "TOU",
            "parent_id" => 10,
            "status_component_block_id" => 2
        ])->create([
            "title" => "RBX",
            "parent_id" => 10,
            "status_component_block_id" => 2
        ]);
    }

    private function subbarstockage(): void
    {
        StatusComponent::create([
            "title" => "BOR",
            "parent_id" => 11,
            "status_component_block_id" => 3
        ])->create([
            "title" => "STR",
            "parent_id" => 11,
            "status_component_block_id" => 3
        ]);

        StatusComponent::create([
            "title" => "BOR",
            "parent_id" => 12,
            "status_component_block_id" => 3
        ])->create([
            "title" => "STR",
            "parent_id" => 12,
            "status_component_block_id" => 3
        ]);

        StatusComponent::create([
            "title" => "RBX",
            "parent_id" => 13,
            "status_component_block_id" => 3
        ]);
    }

    private function subbarreseau(): void
    {
        foreach (Datacenter::all() as $datacenter) {
            $ex = explode('-', $datacenter->name);
            StatusComponent::create([
                "title" => $ex[0],
                "parent_id" => 14,
                "status_component_block_id" => 5
            ]);
        }
    }

    private function subbardns(): void
    {
        foreach (Datacenter::all()->random(rand(1, 5)) as $datacenter) {
            $ex = explode('-', $datacenter->name);
            StatusComponent::create([
                "title" => $ex[0],
                "parent_id" => 15,
                "status_component_block_id" => 6
            ]);
        }

        foreach (Datacenter::all() as $datacenter) {
            $ex = explode('-', $datacenter->name);
            StatusComponent::create([
                "title" => $ex[0],
                "parent_id" => 16,
                "status_component_block_id" => 6
            ]);
        }
    }

    private function subwebssl(): void
    {
        StatusComponent::create([
            "title" => "Certissim",
            "parent_id" => 20,
            "status_component_block_id" => 6
        ])->create([
            "title" => "Let's encrypt",
            "parent_id" => 20,
            "status_component_block_id" => 6
        ]);
    }

    private function subwebdomain(): void
    {
        StatusComponent::create([
            "title" => "Enregistrement",
            "parent_id" => 21,
            "status_component_block_id" => 7
        ])->create([
            "title" => "Renouvellement",
            "parent_id" => 21,
            "status_component_block_id" => 7
        ])->create([
            "title" => "Transfers",
            "parent_id" => 21,
            "status_component_block_id" => 7
        ])->create([
            "title" => "Suppression",
            "parent_id" => 21,
            "status_component_block_id" => 7
        ])->create([
            "title" => "Verification",
            "parent_id" => 21,
            "status_component_block_id" => 7
        ]);
    }

    private function subwebcdninfra(): void
    {
        foreach (Datacenter::all()->random(3) as $datacenter) {
            $ex = explode('-', $datacenter->name);
            StatusComponent::create([
                "title" => $ex[0],
                "parent_id" => 24,
                "status_component_block_id" => 9
            ]);
        }
    }

    private function subwebcdnhost(): void
    {
        foreach (Datacenter::all()->random(3) as $datacenter) {
            $ex = explode('-', $datacenter->name);
            StatusComponent::create([
                "title" => $ex[0],
                "parent_id" => 25,
                "status_component_block_id" => 9
            ]);
        }
    }

    private function subwebdnshost(): void
    {
        for ($i=1; $i <= rand(3, 15); $i++) {
            StatusComponent::create([
                "title" => 'dns'.$i,
                "parent_id" => 26,
                "status_component_block_id" => 10
            ]);
        }
    }

    private function subwebdnsanycast(): void
    {
        StatusComponent::create([
            "title" => 'PAR',
            "parent_id" => 27,
            "status_component_block_id" => 10
        ]);

        StatusComponent::create([
            "title" => 'MAR',
            "parent_id" => 27,
            "status_component_block_id" => 10
        ]);
    }

    private function subwebemail(): void
    {
        StatusComponent::create([
            "title" => 'Reception',
            "parent_id" => 28,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Livraison',
            "parent_id" => 28,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Redirection',
            "parent_id" => 28,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Webmail',
            "parent_id" => 28,
            "status_component_block_id" => 11
        ]);
    }
    private function subwebemailpro(): void
    {
        StatusComponent::create([
            "title" => 'Reception',
            "parent_id" => 29,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Livraison',
            "parent_id" => 29,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Redirection',
            "parent_id" => 29,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Webmail',
            "parent_id" => 29,
            "status_component_block_id" => 11
        ]);
    }
    private function subwebemailCollabor(): void
    {
        StatusComponent::create([
            "title" => 'Accès',
            "parent_id" => 30,
            "status_component_block_id" => 11
        ]);
        StatusComponent::create([
            "title" => 'Livraison',
            "parent_id" => 30,
            "status_component_block_id" => 11
        ]);
    }
    private function subwebemailoffice(): void
    {
        StatusComponent::create([
            "title" => 'Livraison',
            "parent_id" => 31,
            "status_component_block_id" => 11
        ]);
    }
}
