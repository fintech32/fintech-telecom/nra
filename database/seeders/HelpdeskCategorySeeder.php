<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\IT\Helpdesk\HelpdeskCategory;
use Illuminate\Database\Seeder;

final class HelpdeskCategorySeeder extends Seeder
{
    public function run(): void
    {
        HelpdeskCategory::create(["title" => "Suivi de commande et facturation"]);
        HelpdeskCategory::create(["title" => "Conseil technique"]);
        HelpdeskCategory::create(["title" => "Incident"]);
        HelpdeskCategory::create(["title" => "Gérez votre compte"]);
    }
}
