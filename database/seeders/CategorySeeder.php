<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\Category;
use Illuminate\Database\Seeder;

final class CategorySeeder extends Seeder
{
    public function run(): void
    {
        Category::create(["name" => "Bare Metal System"]);
        Category::create(["name" => "Hébergement & Web"]);
        Category::create(["name" => "Télécom"]);
        Category::create(["name" => "Mobile"]);
    }
}
