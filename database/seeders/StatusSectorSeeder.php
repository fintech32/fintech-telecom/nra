<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Core\IT\Status\StatusSector;
use Illuminate\Database\Seeder;

final class StatusSectorSeeder extends Seeder
{
    public function run(): void
    {
        StatusSector::create(["title" => "Bare Metal Cloud"]);
        StatusSector::create(["title" => "Web Cloud"]);
        StatusSector::create(["title" => "Telecom"]);
        StatusSector::create(["title" => "Infrastructure"]);
        StatusSector::create(["title" => "Service client"]);
    }
}
