<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/core')->group(function (): void {
    Route::prefix('category')->group(function (): void {
        Route::get('/', [\App\Http\Controllers\Api\Core\Service\CategoryController::class, 'index']);
        Route::post('/', [\App\Http\Controllers\Api\Core\Service\CategoryController::class, 'store']);
        Route::get('{category_id}', [\App\Http\Controllers\Api\Core\Service\CategoryController::class, 'show']);
        Route::put('{category_id}', [\App\Http\Controllers\Api\Core\Service\CategoryController::class, 'update']);
        Route::delete('{category_id}', [\App\Http\Controllers\Api\Core\Service\CategoryController::class, 'destroy']);

        Route::prefix('{category_id}/sub')->group(function (): void {
            Route::get('/', [\App\Http\Controllers\Api\Core\Service\SubCategoryController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\Core\Service\SubCategoryController::class, 'store']);
            Route::get('{sub_id}', [\App\Http\Controllers\Api\Core\Service\SubCategoryController::class, 'show']);
            Route::put('{sub_id}', [\App\Http\Controllers\Api\Core\Service\SubCategoryController::class, 'update']);
            Route::delete('{sub_id}', [\App\Http\Controllers\Api\Core\Service\SubCategoryController::class, 'destroy']);
        });
    });

    Route::prefix('service')->group(function (): void {
        Route::get('/', [\App\Http\Controllers\Api\Core\Service\ServiceController::class, 'index']);
        Route::post('/', [\App\Http\Controllers\Api\Core\Service\ServiceController::class, 'store']);
        Route::get('{service_id}', [\App\Http\Controllers\Api\Core\Service\ServiceController::class, 'show']);
        Route::put('{service_id}', [\App\Http\Controllers\Api\Core\Service\ServiceController::class, 'update']);
        Route::delete('{service_id}', [\App\Http\Controllers\Api\Core\Service\ServiceController::class, 'destroy']);

        Route::prefix('{service_id}/feature')->group(function (): void {
            Route::get('/', [\App\Http\Controllers\Api\Core\Service\ServiceFeatureController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\Core\Service\ServiceFeatureController::class, 'store']);
            Route::get('{feature_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeatureController::class, 'show']);
            Route::put('{feature_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeatureController::class, 'update']);
            Route::delete('{feature_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeatureController::class, 'destroy']);

            Route::prefix('{feature_id}/price')->group(function (): void {
                Route::get('/', [\App\Http\Controllers\Api\Core\Service\ServiceFeaturePriceController::class, 'index']);
                Route::post('/', [\App\Http\Controllers\Api\Core\Service\ServiceFeaturePriceController::class, 'store']);
                Route::get('{price_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeaturePriceController::class, 'show']);
                Route::put('{price_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeaturePriceController::class, 'update']);
                Route::delete('{price_id}', [\App\Http\Controllers\Api\Core\Service\ServiceFeaturePriceController::class, 'destroy']);
            });
        });
    });
});
